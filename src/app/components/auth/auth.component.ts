import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { AuthService,GoogleLoginProvider } from 'angular-6-social-login';
@Component({
  selector: 'app-user-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  constructor(private socialAuthService: AuthService, private router:Router) { }

  ngOnInit() {
  }

  socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "google"){
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        this.router.navigate(['userdetails'])
        localStorage.setItem("currentUser",JSON.stringify(userData));
      }
    );
  }
}
