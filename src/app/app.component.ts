import { Component } from '@angular/core';
import {Router,NavigationEnd,NavigationStart, Event} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  sub:any;
  currentUrl:any;
  constructor(router: Router){
    this.sub = router.events.subscribe((event : Event) => {
      if (event instanceof NavigationStart ) {
      this.currentUrl = event.url;
      }
    });
  }
}
